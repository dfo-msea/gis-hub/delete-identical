'''
Delete identical features with feature classes or file geodatabase.
'''

# Import modules
import arcpy
import os
from arcpy import env
import logging
from datetime import datetime

# Overwrite existing data.
env.overwriteOutput = True


def get_output_directory(workspace):
    """
    Returns a filepath for output directory.
    :param workspace: input path provided by user (path to directory or file geodatabase)
    :return: output file path string
    """
    if workspace.endswith('gdb'):
        # Move up a directory for output directory if user provided geodatabase
        out_dir = os.path.dirname(workspace)
    else:
        # Set output directory variable
        out_dir = workspace
    return out_dir


def delete_identical(data_workspace, ignore_fields_extra=None, out_dir=None):
    """
    Delete identical records from shapefiles or feature classes in workspace.
    :param data_workspace: path to directory with shapefiles or path to file geodatabase.
    :param ignore_fields_extra: (optional) extra fields to ignore when checking for duplicates.
    :param out_dir: path to output directory (may be different than data_workspace if that is a geodatabase).
    :return:
    """
    # Set workspace
    env.workspace = data_workspace

    # Ignore fields from list
    ignore_fields = ['OBJECTID', 'OID', 'ObjectID', 'FID']

    # Combine lists if extra fields from user provided
    if ignore_fields_extra:
        ignore_fields += ignore_fields_extra

    # Iterate through feature classes/shapefiles in workspace
    for fc in arcpy.ListFeatureClasses():

        # Log
        logging.info('*' * len(fc))
        logging.info(fc)
        logging.info('*' * len(fc))
        orig = arcpy.GetCount_management(fc)
        logging.info(f'Number of features in original: {orig}')

        # get field objects from feature class
        fields_objects = arcpy.ListFields(fc)

        # Get list of field names to pass to delete identical
        field_names = [field.name for field in fields_objects if field.name not in ignore_fields]

        # Log field names to be used for delete identical.
        logging.info(f'The following fields will be used to check for duplicate records: ')
        [logging.info(field) for field in field_names]

        # Find identical records, export csv file.
        if fc.endswith('.shp'):
            fc_name = fc.strip('.shp')
        else:
            fc_name = fc
        # ArcPy cannot export table (csv) that starts with a number.
        if fc_name[0].isdigit():
            fc_name = '_' + fc_name
        arcpy.FindIdentical_management(fc, os.path.join(out_dir, fc_name + '_table.csv'), field_names,
                                       output_record_option='ONLY_DUPLICATES')

        # Delete identical records if any exist.
        arcpy.DeleteIdentical_management(fc, field_names)
        cleaned = arcpy.GetCount_management(fc)
        logging.info(f'Number of features in cleaned (after removing duplicates): {cleaned}')
        logging.info(f'Number of features deleted: {int(orig.getOutput(0)) - int(cleaned.getOutput(0))}')


def main():
    # CLI arguments
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('input_dir', type=str,
                        help='full path to input directory with .shps or .gdb with vector feature classes')
    parser.add_argument('-i', '--ignore', type=str, default=None, nargs='*',
                        help='Additional fields to be ignored (OBJECTID, ObjectID, OID, and FID ignored automatically).'
                             'Space delimited.')

    args = parser.parse_args()

    # Get output directory
    output_dir = get_output_directory(args.input_dir)

    # Datetime
    date_time = datetime.today().strftime('%Y-%m-%d')

    # Set up logging (a+ creates file if it does not exist, or opens it in append mode)
    logging.basicConfig(filename=os.path.join(output_dir, f'delete-duplicates-{date_time}.log'),
                        filemode='a+',
                        format='%(asctime)s:%(levelname)s:%(module)s(%(lineno)d) - %(message)s',
                        level=logging.INFO)
    console = logging.StreamHandler()
    console.setLevel(logging.ERROR)
    logging.getLogger('').addHandler(console)

    if args.ignore:
        logging.info(args.ignore)

    delete_identical(args.input_dir, args.ignore, output_dir)


if __name__ == '__main__':
    main()
