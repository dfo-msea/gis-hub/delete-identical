# delete-identical

__Main author:__  Cole Fields    
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: Cole.Fields@dfo-mpo.gc.ca | tel: 250-363-8060


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
  * [Steps in code](#steps-in-code)
- [Requirements](#requirements)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective
The purpose is to delete duplicate features inside feature classes or shapefiles and report how many (if any) 
were removed as duplicates. This is the Python3 version. See 'Releases' for Python2 version.


## Summary
Script uses ArcPy to get a list of feature classes or shapefiles from a filepath to a directory or file geodatabase. 
Script will loop through feature classes in a file geodatabase or shapefiles and delete any identical features 
based on all fields expect for the unique ID field (ObjectID/OBJECTID/OID/FID). Also exports a csv file per feature 
class with duplicate values (will be empty if no duplicates) and a log file with details in the specified input 
directory (or the parent directory of the geodatabase). The reason this script was written was for QA/QC of datasets 
being published on the GIS Hub. Running it on several of the datasets revealed that many had duplicate features.


## Status
Completed

## Contents
Standalone script meant to be run from the command line interface. User could download the script, and modify it so that 
they hard-code values as parameters.


## Methods
Starts a log file where messages are printed to such as the number of features in the original feature class, number of 
removed features, and the number of features in the new feature class. The script will loop through the feature classes 
in a file geodatabase or shapefiles in a directory and check for duplicate features using 
`arcpy.FindIdentical_management` to export a csv of any duplicate records found. It then uses 
`arcpy.DeleteIdentical_management` to delete the duplicate features. By default, the only field that is ignored is the 
 unique identifier field, but the user can add extra fields to ignore by specifying the `-i` flag followed by space-
 delimited field names.


### Steps in code
* Set environment to file geodatabase or directory.
* Starts a log file in input directory or parent directory of geodatabase.
* Creates list of all fields in feature class/shapefile other than list of fields to ignore. 
* Outputs csvs of duplicate values from the Find Identical arcpy tool based on list of fields.
* Deletes any duplicates based on list of field names with Delete Identical arcpy too.
* Keeps log file with count of features in original and new, noting the number of deleted records for each feature class 
or shapefile.


## Requirements
ArcPy module (recommended to have an environment created with a version of python that has access to the ArcPy module). 
Run the script from the command line. Run `python delete_identical -h` to get the help text for the parameters. 
```
usage: delete_identical.py [-h] [-i [IGNORE [IGNORE ...]]] input_dir

positional arguments:
  input_dir             full path to input directory with .shps or .gdb with
                        vector feature classes

optional arguments:
  -h, --help            show this help message and exit
  -i [IGNORE [IGNORE ...]], --ignore [IGNORE [IGNORE ...]]
                        Additional fields to be ignored (OBJECTID, ObjectID,
                        OID, and FID ignored automatically).Space delimited.
```

Examples:

`python delete_identical.py C:\Temp\shapefile_dir -i Latitude Longitude`

`python delete_identical.py C:\Temp\precious_data.gdb -i Latitude Longitude`

## Caveats
* If no python 3 env created with access to ArcPy module, user must specify full path to python executable in the ArcGIS 
installation folder. Example: 
`C:\Program Files\ArcGIS\Pro\bin\Python\envs\arcgispro-py3\python.exe delete_identical.py -i Lat Lon`.
* Only set up to work with a shapefiles in a directory or file geodatabase.
* No error handling set up.
* Recommended to run this on a copy of the data and keep the original in case the script fails.


## Uncertainty 
User will need to know if there are extra fields that need to be ignored (besides unique ID field) when running the
script, otherwise the script check for duplicates using all other fields.


## Acknowledgements 
NA


## References 
NA

